/**
 * 1. Напиши функцию createPromise, которая будет возвращать промис
 * */

function createPromise() {
  return new Promise((resolve, reject) => {});
}

/**
 * 2. Напиши функцию createResolvedPromise, которая будет возвращать промис, который успешно выполнен (fulfilled)
 * */

function createResolvedPromise() {
  return new Promise((resolve, reject) => {
    resolve();
  });
}

/**
 * 3. Напиши функцию createResolvedPromiseWithData, которая будет возвращать промис, который успешно выполнен
 * (fulfilled) и возвращать при резолве объект вида  { success: true }
 * */

function createResolvedPromiseWithData() {
  return new Promise((resolve, reject) => {
    resolve({ success: true });
  });
}

/**
 * 4. Напиши функцию createRejectedPromise, которая будет возвращать промис, который будет отклонен (rejected)
 * */

function createRejectedPromise() {
  return new Promise((resolve, reject) => {
    reject();
  });
}

/**
 * 5. Напиши функцию createRejectedPromiseWithError, которая будет возвращать промис, который будет отклонен
 * (rejected) и отклоненыый промис должен возвращать объект ошибки с текстом "Что-то пошло не так"
 * */

function createRejectedPromiseWithError() {
  return new Promise((resolve, reject) => {
    reject(new Error('Что-то пошло не так'));
  });
}

/**
 * 6. Напиши функцию fulfilledOrNotFulfilled, которая принимает на вход boolean аргумент и возвращает промис.
 * Если аргумент true, то промис должен быть выполнен, иначе — отклонен.
 * */

function fulfilledOrNotFulfilled(shouldBeResolve) {
  return new Promise((resolve, reject) => {
    return shouldBeResolve ? resolve() : reject();
  });
}

/**
 * 7. Создай функцию timer, которая возвращает промис, который выполняется через 3 секунды.
 * */

function timer() {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), 3000);
  });
}

/**
 * 8. Используя async/await, напиши асинхронную функцию. Функция на вход принимает boolean аргумент и передает его в функцию из задания 6.
 * Если функция из задания 6 была зарезолвлена, возвращай 'success'.
 * Если зареджекчена, возвращай 'error'
 */

function callOneAsyncFunction(shouldBeResolve) {
  const makeRequest = async () => {
    try {
      const resultfrom6 = await fulfilledOrNotFulfilled(shouldBeResolve);
      return 'success';
    } catch (err) {
      return 'error';
    }
  };
  return makeRequest();
}

/**
 * 9. Используя async/await, напиши асинхронную функцию. Функция должна получать результат из функции задания 3 и
 * передавать его в функцию из задания 6. Если функция из задания 6 была зарезолвлена, возвращай 'success'.
 */

function callManyAsyncFunction() {
  const makeRequest = async () => {
    try {
      const resultfrom3 = await createResolvedPromiseWithData();
      await fulfilledOrNotFulfilled(resultfrom3);
      return 'success';
    } catch (err) {
      return 'nesuccess';
    }
  };
  return makeRequest();
}

module.exports = {
  createPromise,
  createResolvedPromise,
  createResolvedPromiseWithData,
  createRejectedPromise,
  createRejectedPromiseWithError,
  fulfilledOrNotFulfilled,
  timer,
  callOneAsyncFunction,
  callManyAsyncFunction,
};
